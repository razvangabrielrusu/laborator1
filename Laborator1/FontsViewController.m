//
//  ViewController.m
//  Laborator1
//
//  Created by Razvan Rusu on 17/02/15.
//  Copyright (c) 2015 Razvan Rusu. All rights reserved.
//

#import "FontsViewController.h"
#import "FontDetailViewController.h"

@interface FontsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *mainTableView;
@property (strong, nonatomic) NSMutableArray *fontFamilies;
@property (strong, nonatomic) NSMutableArray *fontNames;

@end

@implementation FontsViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"Fonts";
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 49 - 44 - 20) style:UITableViewStyleGrouped];
    [_mainTableView setDelegate:self];
    [_mainTableView setDataSource:self];
    [_mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.view addSubview:_mainTableView];

    // List all fonts on iPhone
    _fontFamilies = [NSMutableArray arrayWithArray:[UIFont familyNames]];
    [_fontFamilies sortUsingSelector:@selector(localizedCompare:)];
    
    _fontNames = [NSMutableArray array];
    
    for(NSUInteger idx = 0; idx < [_fontFamilies count]; idx++)
    {
        [_fontNames insertObject:[UIFont fontNamesForFamilyName:(NSString *)_fontFamilies[idx]] atIndex:idx];
    }
    
    [_mainTableView reloadData];

    // Do any additional setup after loading the view, typically from a nib.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_fontNames.count > 0)
    {
        return [_fontNames[section] count];
    }
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
 
   if(_fontFamilies.count > 0)
    {
        return [_fontFamilies count];
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (_fontFamilies.count > 0)
    {
        return (NSString *)_fontFamilies[section];
    }
    return @"";
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FontDetailViewController *fontDetailViewController = [[FontDetailViewController alloc] initWithDetailFont:_fontNames[indexPath.section][indexPath.row]];
    [self.navigationController pushViewController:fontDetailViewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(_fontNames.count > 0)
    {
        [cell.textLabel setFont:[UIFont fontWithName:(NSString *)_fontNames[indexPath.section][indexPath.row] size:15]];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = (NSString *)_fontNames[indexPath.section][indexPath.row];
    }
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
