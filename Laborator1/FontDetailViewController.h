//
//  FontDetailViewController.h
//  Laborator1
//
//  Created by Razvan Rusu on 18/02/15.
//  Copyright (c) 2015 Razvan Rusu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FontDetailViewController : UIViewController

@property (strong, nonatomic) NSString *detailFontName;

- (instancetype)initWithDetailFont:(NSString *)detailFontName;

@end
