//
//  CountriesViewController.m
//  Laborator1
//
//  Created by Razvan Rusu on 17/02/15.
//  Copyright (c) 2015 Razvan Rusu. All rights reserved.
//

#import "CountriesViewController.h"

@interface CountriesViewController() <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *mainTableView;
@property (strong, nonatomic) NSMutableArray *countryArray;

@end

@implementation CountriesViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"Countries";
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height  - 49 - 44 - 20) style:UITableViewStylePlain];
    [_mainTableView setDelegate:self];
    [_mainTableView setDataSource:self];
    [_mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.view addSubview:_mainTableView];
    
    NSLocale *locale = [NSLocale systemLocale];
    NSArray *isoCountryCodes = [NSLocale ISOCountryCodes];
    
    _countryArray = [NSMutableArray  new];
    
    for (NSString *countryCode in isoCountryCodes)
    {
        NSString *countryName = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        [_countryArray addObject:countryName];
    }
    
    
    [_countryArray sortUsingSelector:@selector(localizedCompare:)];
    // List all fonts on iPhone
    [_mainTableView reloadData];
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_countryArray count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove the row from data model
    
    [_countryArray removeObjectAtIndex:indexPath.row];
    
    // Request table view to reload
    [tableView reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if([_countryArray count] > 0)
    {
        [cell.textLabel setFont:[UIFont fontWithName:@"AvenirNext-Regular" size:15]];
        cell.textLabel.text = (NSString *)_countryArray[indexPath.row];
    }
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
