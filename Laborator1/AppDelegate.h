//
//  AppDelegate.h
//  Laborator1
//
//  Created by Razvan Rusu on 17/02/15.
//  Copyright (c) 2015 Razvan Rusu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FontsViewController.h"
#import "CountriesViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) FontsViewController *fontsViewController;
@property (strong, nonatomic) CountriesViewController *countriesViewController;
@property (strong, nonatomic) UITabBarController *tabBarController;
@end

