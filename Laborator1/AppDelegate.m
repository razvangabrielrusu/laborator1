//
//  AppDelegate.m
//  Laborator1
//
//  Created by Razvan Rusu on 17/02/15.
//  Copyright (c) 2015 Razvan Rusu. All rights reserved.
//

#import "AppDelegate.h"


@interface AppDelegate ()

@end

#define kPostFontSize 12.0

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    _window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    [self initTabBarController];
    [self configureNavigationBar];
    
    [_window setRootViewController:_tabBarController];
    [_window makeKeyAndVisible];
    // Override point for customization after application launch.
    return YES;
}

- (void)initTabBarController
{
    _tabBarController = [[UITabBarController alloc] init];
    
    [_tabBarController setViewControllers:[self _mainViewControllersArray]];
    [_tabBarController.tabBar setTranslucent:NO];
    
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:1 green:0.804 blue:0.341 alpha:1]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-DemiBold" size:kPostFontSize],
                                                         NSForegroundColorAttributeName : [UIColor whiteColor] }
                                             forState:UIControlStateSelected];
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-DemiBold" size:12.0f],
                                                         NSForegroundColorAttributeName : [UIColor colorWithRed:0.29 green:0.227 blue:0.278 alpha:1] }
                                             forState:UIControlStateNormal];
    
     [self _configureTabBarItems];
    

}

- (NSArray *)_mainViewControllersArray
{
    _fontsViewController = [[FontsViewController alloc] init];
    _fontsViewController.title = @"Fonts";
    _countriesViewController = [[CountriesViewController alloc] init];
    _countriesViewController.title = @"Countries";
    
    UINavigationController *fontsNavigationController = [[UINavigationController alloc] initWithRootViewController:_fontsViewController];
    UINavigationController *countriesNavigationController = [[UINavigationController alloc] initWithRootViewController:_countriesViewController];
    
    return @[fontsNavigationController, countriesNavigationController];
}

// Configuring Tab Bar Items Images

- (void)_configureTabBarItems
{

    UITabBarItem *item0 = [self.tabBarController.tabBar.items objectAtIndex:0];
    
    // this way, the icon gets rendered as it is (thus, it needs to be white in this example)
    item0.image = [[UIImage imageNamed:@"listSel"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    // this icon is used for selected tab and it will get tinted as defined in self.tabBarController.tabBar.tintColor
    item0.selectedImage = [UIImage imageNamed:@"list"];
    
    UITabBarItem *item1 = [self.tabBarController.tabBar.items objectAtIndex:1];
    
    // this way, the icon gets rendered as it is (thus, it needs to be white in this example)
    item1.image = [[UIImage imageNamed:@"flagSel"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    // this icon is used for selected tab and it will get tinted as defined in self.tabBarController.tabBar.tintColor
    item1.selectedImage = [UIImage imageNamed:@"flag"];
    
    [self.tabBarController setSelectedIndex:0];
    [self.tabBarController.tabBar setTintColor:[UIColor whiteColor]];
}

- (void)configureNavigationBar
{
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName: [UIColor colorWithRed:0.29 green:0.227 blue:0.278 alpha:1],
                                                           NSFontAttributeName: [UIFont fontWithName:@"AvenirNext-DemiBold" size:16.0],
                                                           }];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:1 green:0.804 blue:0.341 alpha:1]];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
