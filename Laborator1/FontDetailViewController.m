//
//  FontDetailViewController.m
//  Laborator1
//
//  Created by Razvan Rusu on 18/02/15.
//  Copyright (c) 2015 Razvan Rusu. All rights reserved.
//

#import "FontDetailViewController.h"

#define kTextSampleSentence @"The quick brown fox jumps over the lazy dog"
#define kTextSampleAllCharacters @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()_+[];'\\,./{}:\"|<>?"

@interface FontDetailViewController()

@property (strong, nonatomic) UITextView *detailTextView;

@end

@implementation FontDetailViewController


- (instancetype)initWithDetailFont:(NSString *)detailFontName
{
    if(self = [super init])
    {
        _detailFontName = detailFontName;
        self.title = detailFontName;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.29 green:0.227 blue:0.278 alpha:1];
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor = [UIColor whiteColor];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    _detailTextView = [[UITextView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, self.view.frame.size.height)];
    _detailTextView.text = [NSString stringWithFormat:@"%@\n\n%@",kTextSampleAllCharacters, kTextSampleSentence];
    _detailTextView.userInteractionEnabled = NO;
    _detailTextView.editable = NO;
    [_detailTextView setFont:[UIFont fontWithName:_detailFontName size:20]];
    [self.view addSubview:_detailTextView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
